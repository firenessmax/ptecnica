from flask import Flask
from FonasaApp.api.routes import mod as api_mod
from FonasaApp.site.home.routes import mod as home_mod
import os

def create_app():

    app = Flask(__name__)

    #####################################################
    # Create database resources                         #
    #####################################################
    from FonasaApp.api.models import db as admin_db
    basedir = os.path.abspath(os.path.dirname(__file__))
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////'+os.path.join(basedir, 'bkp.sqlite')
    if 'FONASAAPP_BD_CONECTION_STRING' in os.environ:
        app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['FONASAAPP_BD_CONECTION_STRING']
    # Setting this to false due to warning it was deprecated and being removed in a future release
    # so will not use it at all
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'mysecret'

    #####################################################
    # Init app databases.
    #####################################################
    admin_db.init_app(app)
    #####################################################
    # Register blueprints                               #
    #####################################################
    app.register_blueprint(home_mod)
    app.register_blueprint(api_mod, url_prefix="/api")
    return app
