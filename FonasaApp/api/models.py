from flask_sqlalchemy import SQLAlchemy
from FonasaApp.models.paciente import PNino,PJoven,PAnciano
db = SQLAlchemy()

class Paciente(db.Model):
     nombre = db.Column(db.String(30))
     no_historia_clinica = db.Column(db.Integer,primary_key=True)
     edad = db.Column(db.Integer)
     tiempo_fumador = db.Column(db.Integer)
     peso_estatura = db.Column(db.Float)
     pendiente = db.Column(db.Boolean)
     tiene_dieta = db.Column(db.Boolean)
     hospital_id = db.Column(db.Integer, db.ForeignKey('hospital.id'))

     def as_paciente(self):
         if 0 < self.edad < 16:
            paciente = PNino(peso_estatura=self.peso_estatura, nombre = self.nombre, noHistoriaClinica=self.no_historia_clinica, edad=self.edad)
            return paciente
         elif self.edad < 60:
             paciente = PJoven(tiempo_fumador=self.tiempo_fumador, nombre=self.nombre, noHistoriaClinica=self.no_historia_clinica, edad=self.edad)
             return paciente
         else:
             paciente = PAnciano(tiene_dieta=self.tiene_dieta, nombre=self.nombre, noHistoriaClinica=self.no_historia_clinica, edad=self.edad)
             return paciente

class Hospital(db.Model):
    nombre = db.Column(db.String(30))
    id = db.Column(db.Integer, primary_key=True)
    consultas = db.relationship('Consulta', backref='hospital', lazy=True)
    pacientes = db.relationship('Paciente', backref='hospital', lazy=True)

class Consulta(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre_especialista = db.Column(db.String(30))
    cantidad_pacientes = db.Column(db.Integer)
    ocupado = db.Column(db.Boolean)
    tipo = db.Column(db.Integer)
    hospital_id = db.Column(db.Integer, db.ForeignKey('hospital.id'))
    def tipo_consulta(self):
        return ['Urgencia', 'CGI', 'Pediatria'][self.tipo-1]