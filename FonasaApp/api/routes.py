from flask import Flask, render_template, request, redirect, jsonify, url_for, flash, Blueprint
from flask_sqlalchemy import SQLAlchemy, inspect
from FonasaApp.api.models import Paciente, Consulta
import json

db = SQLAlchemy()
mod = Blueprint('api', __name__)

DE_LA_SALA_DE_ESPERA = {"pendiente": False}


# Listar_Pacientes_Mayor_Riesgo
@mod.route('/<hospital_id>/pacientes/mas_riesgo_que/<noHistoria>', methods=['GET'])
def mas_riesgo_que(hospital_id, noHistoria):
    paciente = Paciente.query.filter_by(hospital_id=hospital_id, no_historia_clinica=noHistoria).first()
    if paciente:
        logicPaciente = paciente.as_paciente()
        pacientesMap = map(lambda p: p.as_paciente(), Paciente.query.yield_per(100))
        pacientesFiltered = filter(lambda p: p.riesgo > logicPaciente.riesgo, pacientesMap)
        pacientesAsDict = map(lambda p: p.__dict__(), pacientesFiltered)
        return jsonify(list(pacientesAsDict))
    else:
        return jsonify(message="no encontrado"), 404


# Listar_Pacientes_Fumadores_Urgentes
@mod.route('/<hospital_id>/pacientes/fumadores_urgentes', methods=['GET'])
def fumadores_urgentes(hospital_id):
    paciente = Paciente.query.filter \
        ((Paciente.hospital_id == hospital_id) & (Paciente.tiempo_fumador > 0))
    pacientesMap = map(lambda p: p.as_paciente(), paciente.yield_per(100))
    pacientesFiltered = filter(lambda p: p.prioridad > 4, pacientesMap)
    pacientesAsDict = map(lambda p: p.__dict__(), pacientesFiltered)
    return jsonify(list(pacientesAsDict))


# Paciente_Mas_Anciano
@mod.route('/<hospital_id>/pacientes/mas_anciano', methods=['GET'])
def paciente_mas_anciano(hospital_id):
    paciente = Paciente.query.filter_by(hospital_id=hospital_id, **DE_LA_SALA_DE_ESPERA).order_by("edad desc").first()
    return jsonify(paciente.as_paciente().__dict__())


def liberar(hospital_id):
    db.session.query(Consulta).filter_by(hospital_id=hospital_id).update({'ocupado': False})
    db.session.commit()

def pasar_a_sala_espera(hospital_id):
    db.session.query(Paciente).filter_by(hospital_id=hospital_id, pendiente=1).update({'pendiente': False})
    db.session.commit()

def atender(hospital_id):
    if Paciente.query.filter_by(**DE_LA_SALA_DE_ESPERA).count() == 0:
        pasar_a_sala_espera(hospital_id)
    consultas_libres = Consulta.query.filter_by(ocupado=False).yield_per(100)
    consultas = {'CGI': [], 'Urgencia': [], 'Pediatria': []}
    for consulta in consultas_libres:
        consultas[consulta.tipo_consulta()].append(consulta)
    pacientesMap = map(lambda p: p.as_paciente(),
                       Paciente.query.filter_by(hospital_id=hospital_id, **DE_LA_SALA_DE_ESPERA).yield_per(100))
    pacientesSorted = sorted(pacientesMap, key=lambda p: -p.prioridad)
    count = 0
    for paciente in pacientesSorted:
        if len(consultas[paciente.tipo_consulta]) > 0:
            count += 1
            consulta = consultas[paciente.tipo_consulta].pop(0)
            consulta.ocupado = True
            consulta.cantidad_pacientes += 1
            session = inspect(consulta).session
            session.query(Paciente).filter(Paciente.no_historia_clinica == paciente._noHistoriaClinica).delete()
            session.commit()
    return count


# Atender_Paciente
@mod.route('/<hospital_id>/consulta/atender', methods=['POST'])
def atender_pacientes(hospital_id):
    count = atender(hospital_id)
    return jsonify(pacientes_atendidos=count)


# Liberar_Consultas
@mod.route('/<hospital_id>/liberar_consultas', methods=['POST'])
def liberar_consulta(hospital_id):
    liberar(hospital_id)
    count = atender(hospital_id)
    return jsonify(pacientes_atendidos=count)


# Consulta_mas_Pacientes_Atendidos
@mod.route('/<hospital_id>/consulta/mas_pacientes_atendidos', methods=['GET'])
def mas_pacientes_atendidos(hospital_id):
    print(Paciente.query.filter_by(hospital_id=hospital_id, **DE_LA_SALA_DE_ESPERA).count())
    consulta = Consulta.query.order_by("cantidad_pacientes desc").first()
    return jsonify({p.name: consulta.__getattribute__(p.name) for p in consulta.__mapper__.columns})


# Optimizar_Atención
@mod.route('/<hospital_id>/optimizar_atencion', methods=['POST'])
def optimizar_atencion(hospital_id):
    pasar_a_sala_espera(hospital_id)
    liberar(hospital_id)
    count = atender(hospital_id)
    return jsonify(pacientes_atendidos=count)