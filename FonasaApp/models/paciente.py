#!/usr/bin/python
# -*- coding: utf-8 -*-

from abc import ABC
from abc import abstractmethod


class Paciente(ABC):
    @property
    @abstractmethod
    def prioridad(self):
        return NotImplemented

    @property
    @abstractmethod
    def riesgo(self):
        return NotImplemented

    def __init__(self, nombre: str, edad: int, noHistoriaClinica: int,*args, **kwargs):
        self._nombre = nombre
        self._edad = edad
        self._noHistoriaClinica = noHistoriaClinica
        super(Paciente, self).__init__(*args, **kwargs)
        
    def __dict__(self):
        return dict(
            nombre=self._nombre,
            edad=self._edad,
            no_historia_Clinica=self._noHistoriaClinica,
            prioridad=self.prioridad
        )

    @property
    def tipo_consulta(self):
        if self.prioridad>4:
            return 'Urgencia'
        else:
            return self._default

class PAnciano(Paciente):
    """docstring for PAnciano"""

    @property
    def tiene_dieta(self):
        return self._tiene_dieta

    @tiene_dieta.setter
    def tiene_dieta(self, value: bool):
        self._tiene_dieta = value
        
    @property
    def prioridad(self):
        if self.tiene_dieta and 60 < self._edad < 100:
            return self._edad / 20.0 + 4
        else:
            return self._edad / 30.0 + 3

    @property
    def riesgo(self):
        return (self._edad * self.prioridad) / 100.0 + 5.3

    def __init__(self,tiene_dieta:bool, *args,**kwargs):
        super(PAnciano, self).__init__(*args,**kwargs)
        self._tiene_dieta = tiene_dieta
        self._default = 'CGI'

class PJoven(Paciente):
    """docstring for PAnciano"""
    @property
    def es_fumador(self):
        return self._tiempo_fumador > 0

    @property
    def tiempo_fumador(self):
        return self._tiempo_fumador

    @tiempo_fumador.setter
    def tiempo_fumador(self, value: int):
        self._tiempo_fumador = value

    @property
    def prioridad(self):
        if self.es_fumador:
            return self._tiempo_fumador / 4 + 2
        else:
            return 2
    @property
    def riesgo(self):
        return (self._edad * self.prioridad) / 100.0

    def __init__(self, tiempo_fumador: int, *args, **kwargs):
        super(PJoven, self).__init__(*args, **kwargs)
        self._tiempo_fumador = tiempo_fumador
        self._default = 'CGI'


class PNino(Paciente):
    """docstring for PAnciano"""
    @property
    def peso_estatura(self):
        return self._peso_estatura

    @peso_estatura.setter
    def peso_estatura(self, value: float):
        self._peso_estatura = value

    @property
    def prioridad(self):
        if 1 <= self._edad <= 5:
            return self.peso_estatura + 3
        elif 2 <= self._edad <= 12:
            return self.peso_estatura + 2
        else:
            return self.peso_estatura + 1

    @property
    def riesgo(self):
        return (self._edad * self.prioridad) / 100.0

    def __init__(self,peso_estatura:float, *args,**kwargs):
        super(PNino, self).__init__(*args,**kwargs)
        self._peso_estatura = peso_estatura
        self._default = 'Pediatria'
