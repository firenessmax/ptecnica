import random
from flask import Flask, render_template, request, redirect, jsonify, url_for, flash, Blueprint
from FonasaApp.api.models import Hospital,Paciente
mod = Blueprint('site', __name__, template_folder="templates", static_folder="static",  static_url_path='/site/home/static')


@mod.route('/')
def index():
    return render_template('index.html', hospitales = Hospital.query.all())
    # return render_template('in_development.html')

@mod.route('/sala_espera/<hospital_id>')
def sala_espera(hospital_id):
    pacientesMap = map(lambda p: p.as_paciente(),
                       Paciente.query.filter((Paciente.pendiente == False) & (Paciente.hospital_id==hospital_id)).all())
    pacientesSorted = sorted(pacientesMap, key=lambda p: -p.prioridad)
    pendientesMap = map(lambda p: p.as_paciente(),
                       Paciente.query.filter((Paciente.pendiente) & (Paciente.hospital_id == hospital_id)).all())
    pendientesSorted = sorted(pendientesMap, key=lambda p: -p.prioridad)
    salaEspera = list(pacientesSorted)
    return render_template('sala_espera.html',
                           hospital=Hospital.query.filter_by(id=hospital_id).first(),
                           pacientes=salaEspera,
                           primer_paciente = random.choice(salaEspera),
                           pendientes=list(pendientesSorted))



