# FonasaAPP

Se crearon los servicios para cumplir con todos los requicitos listados:

- Listar_Pacientes_Mayor_Riesgo
- Atender_Paciente
- Liberar_Consultas
- Listar_Pacientes_Fumadores_Urgentes
- Consulta_mas_Pacientes_Atendidos
- Paciente_Mas_Anciano
- Optimizar_Atención 

Del modelo propuesto en el enunciado se variaron algunos campos:

![modelo](modelo_bd.png)

Los campos de cada tipo de paciente fueron unificados en una sola tabla, y se agregó un campo extra *pendiente* 
que indica si un paciente se encuentra en la sala de espera o en la sala de pendientes.

Se generan 2 carpetas de modelo, uno especifico para la conexión mediante SQLAlchemy y un modelo independiente el cual
es reutilizable en otros tipos de proyecto. El modelo de tipo SQLAlchemy se construye con los metodos necesarios para la
transformación al modelo reusable.

## Aplicación
Se Hizo una aplicación python 3.5+ con el módulo Flask usando Blueprint para mayor modularidad. La aplicación consta de 1 api con las funciones 
solicitadas y 1 página web simple de ejemplo que permite ver los cambios realizados en el flujo más un ejemplo de las
llamadas.

### Como ejecutar
La aplicación puede ser ejecutada directamente o usando docker.
#### Ejecución directa

*Se sugiere el uso de virtualenv

Primero se necesita instalar las dependencias

```bash
pip install -r requirements.txt
python run.py 
```

Usando estos dos pasos la aplicación correrá en modo ejemplo conectado a una BD sqlite preparada para esto. Si se desea 
usar otro motor de base de datos basta con cambiar la variable de entorno FONASAAPP_BD_CONECTION_STRING

```bash
export FONASAAPP_BD_CONECTION_STRING=postgresql://user:pass@localhost:5432/mydatabase
python run.py 
```

para más información revisar la [documentación](https://docs.sqlalchemy.org/en/13/core/engines.html)

#### Ejecución usando Docker

Para la creación del contenedor se utilizó Docker multistage ya que crea imagenes más pequeñas al reducir la cantidad de
archivos fuentes que se tienen en el contenedor.

Para construir la imagen Docker basta con el comando de construcción Docker sin necesidad de parametros extra.

```bash
docker build -t nmora/FonasaAPP .
```

Como la imagen crea una aplicación Flask productiva para ejecutar el contenedor se debe usar:

```bash
docker run -p 5000:8000/tcp -it nmora/FonasaAPP
```

Para cambiar el motor de BD es similar a la versión Bash

```bash
docker run -e FONASAAPP_BD_CONECTION_STRING=postgresql://user:pass@localhost:5432/mydatabase -p 5000:8000/tcp -it nmora/FonasaAPP
```
